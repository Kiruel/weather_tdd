import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_tdd/features/weather/presentation/bloc/bloc.dart';
import 'package:weather_tdd/features/weather/presentation/bloc/weather_bloc.dart';
import 'package:weather_tdd/features/weather/presentation/widgets/loading_widget.dart';
import 'package:weather_tdd/features/weather/presentation/widgets/message_display.dart';
import 'package:weather_tdd/features/weather/presentation/widgets/weather_controls.dart';
import 'package:weather_tdd/features/weather/presentation/widgets/weather_display.dart';

import '../../../../injection_container.dart';

class WeatherPage extends StatelessWidget {
  const WeatherPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Weather City'),
        centerTitle: true,
      ),
      body: buildBody(context),
    );
  }

  BlocProvider<WeatherBloc> buildBody(BuildContext context) {
    return BlocProvider(
      create: (_) => sl<WeatherBloc>(),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 10,
              ),
              BlocBuilder<WeatherBloc, WeatherState>(
                builder: (BuildContext context, WeatherState state) {
                  if (state is Loading) {
                    return LoadingWidget();
                  } else if (state is Loaded) {
                    return WeatherDisplay(
                      weather: state.weather,
                    );
                  } else if (state is Error) {
                    return MessageDisplay(
                      message: state.message,
                    );
                  } else {
                    return MessageDisplay(
                      message: 'Start searching!',
                    );
                  }
                },
              ),
              SizedBox(height: 20,),
              WeatherControls()
            ],
          ),
        ),
      ),
    );
  }
}
