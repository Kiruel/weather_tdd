import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_tdd/features/weather/presentation/bloc/weather_bloc.dart';
import 'package:weather_tdd/features/weather/presentation/bloc/weather_event.dart';

class WeatherControls extends StatefulWidget {
  WeatherControls({Key key}) : super(key: key);

  _WeatherControlsState createState() => _WeatherControlsState();
}

class _WeatherControlsState extends State<WeatherControls> {
  final controller = TextEditingController();
  String inputStr;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          TextField(
            controller: controller,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Search a City',
            ),
            onChanged: (value) {
              inputStr = value;
            },
            onSubmitted: (_) {
              dispatchCity();
            },
          ),
          SizedBox(
            height: 16,
          ),
          Container(
            width: double.infinity,
            child: ElevatedButton(
              child: Text('Search'),
              style: ElevatedButton.styleFrom(
                onPrimary: Theme.of(context).accentColor,
                primary: Colors.grey[300],
                minimumSize: Size(88, 36),
                padding: EdgeInsets.symmetric(horizontal: 16),
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(2)),
                ),
              ),
              onPressed: dispatchCity,
            ),
          ),
        ],
      ),
    );
  }

  void dispatchCity() {
    controller.clear();
    BlocProvider.of<WeatherBloc>(context).add(GetWeatherForCity(inputStr));
  }
}
