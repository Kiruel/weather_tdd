import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:weather_tdd/core/error/failures.dart';
import 'package:weather_tdd/features/weather/domain/usercases/get_weather_city.dart';
import './bloc.dart';
import 'package:meta/meta.dart';

const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String NOT_FOUND_FAILURE_MESSAGE = 'City not found';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  final GetWeatherCity getWeatherCity;

  WeatherBloc({
    @required GetWeatherCity city,
  })  : assert(city != null),
        getWeatherCity = city,
        super(Empty());

  @override
  Stream<WeatherState> mapEventToState(
    WeatherEvent event,
  ) async* {
    if (event is GetWeatherForCity) {
      yield Loading();
      final failureOrWeather = await getWeatherCity(
        Params(city: event.city),
      );
      yield failureOrWeather.fold(
        (failure) {
          print('FAILURE');
          return Error(message: _malFailureToMessage(failure));
        },
        (weather) {
          print('NOT_FAIL');
          return Loaded(weather: weather);
        },
      );
    }
  }

  String _malFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case NoLocalDataFailure:
        return CACHE_FAILURE_MESSAGE;
      case NotFoundFailure:
        return NOT_FOUND_FAILURE_MESSAGE;
      default:
        return 'Unexpected Error';
    }
  }
}
