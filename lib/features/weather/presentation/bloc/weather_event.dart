import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class WeatherEvent extends Equatable {
  const WeatherEvent();
}

class GetWeatherForCity extends WeatherEvent {
  final String city;

  GetWeatherForCity(this.city);

  @override
  List<Object> get props => [city];
}
