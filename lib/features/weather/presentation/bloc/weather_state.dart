import 'package:equatable/equatable.dart';
import 'package:weather_tdd/features/weather/domain/entities/weather.dart';
import 'package:meta/meta.dart';

@immutable
abstract class WeatherState extends Equatable {
  const WeatherState();

  @override
  List<Object> get props => [];
}

class Empty extends WeatherState {}

class Loading extends WeatherState {}

class Loaded extends WeatherState {
  final Weather weather;

  Loaded({@required this.weather});

  @override
  List<Object> get props => [weather];
}

class Error extends WeatherState {
  final String message;

  Error({@required this.message});

  @override
  List<Object> get props => [message];
}
