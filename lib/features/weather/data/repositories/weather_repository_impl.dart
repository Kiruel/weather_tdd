import 'package:dartz/dartz.dart';
import 'package:weather_tdd/core/error/exception.dart';
import 'package:weather_tdd/core/error/failures.dart';
import 'package:weather_tdd/core/network/network_info.dart';
import 'package:weather_tdd/features/weather/data/datasources/weather_local_data_source.dart';
import 'package:weather_tdd/features/weather/data/datasources/weather_remote_data_source.dart';
import 'package:weather_tdd/features/weather/domain/entities/weather.dart';
import 'package:weather_tdd/features/weather/domain/repositories/weather_repository.dart';
import 'package:meta/meta.dart';

class WeatherRepositoryImpl implements WeatherRepository {
  final WeatherRemoteDataSource remoteDataSource;
  final WeatherLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  WeatherRepositoryImpl({
    @required this.remoteDataSource,
    @required this.localDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, Weather>> getWeatherCity(String city) async {
    if (await networkInfo.isConnected) {
      print('networkInfo.isConnected TRUE');
      try {
        final result = await remoteDataSource.getWeatherCity(city);
        localDataSource.cacheWeather(result);
        print(result);
        return Right(result);
      } on NotFoundException {
        return Left(NotFoundFailure());
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      print('networkInfo.isConnected FALSE');
      try {
        final result = await localDataSource.getLastWeather();
        return Right(result);
      } on NoLocalDataException {
        return Left(NoLocalDataFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }
}
