import 'package:weather_tdd/features/weather/domain/entities/weather.dart';
import 'package:meta/meta.dart';

class WeatherModel extends Weather {
  WeatherModel({
    @required CoordModel coord,
    @required List<WeatherElementModel> weather,
    @required String base,
    @required MainModel main,
    @required int visibility,
    @required WindModel wind,
    @required CloudsModel clouds,
    @required int dt,
    @required SysModel sys,
    @required int id,
    @required String name,
    @required int cod,
  }) : super(
          coord: coord,
          weather: weather,
          base: base,
          main: main,
          visibility: visibility,
          wind: wind,
          clouds: clouds,
          dt: dt,
          sys: sys,
          id: id,
          name: name,
          cod: cod,
        );

  factory WeatherModel.fromJson(Map<String, dynamic> json) {
    return WeatherModel(
      coord: CoordModel.fromJson(json["coord"]),
      weather: List<WeatherElementModel>.from(
          json["weather"].map((x) => WeatherElementModel.fromJson(x))),
      base: json["base"],
      main: MainModel.fromJson(json["main"]),
      visibility: json["visibility"],
      wind: WindModel.fromJson(json["wind"]),
      clouds: CloudsModel.fromJson(json["clouds"]),
      dt: json["dt"],
      sys: SysModel.fromJson(json["sys"]),
      id: json["id"],
      name: json["name"],
      cod: json["cod"],
    );
  }

  Map<String, dynamic> toJson() => {
        "coord": CoordModel(
          lat: coord.lat,
          lon: coord.lon,
        ).toJson(),
        "weather": List<dynamic>.from(weather.map((x) => WeatherElementModel(
              id: x.id,
              description: x.description,
              icon: x.icon,
              main: x.main,
            ).toJson())),
        "base": base,
        "main": MainModel(
          humidity: main.humidity,
          pressure: main.pressure,
          temp: main.temp,
          tempMax: main.tempMax,
          tempMin: main.tempMin,
        ).toJson(),
        "visibility": visibility,
        "wind": WindModel(
          deg: wind.deg,
          speed: wind.speed,
        ).toJson(),
        "clouds": CloudsModel(
          all: clouds.all,
        ).toJson(),
        "dt": dt,
        "sys": SysModel(
          country: sys.country,
          id: sys.id,
          message: sys.message,
          sunrise: sys.sunrise,
          sunset: sys.sunset,
          type: sys.type,
        ).toJson(),
        "id": id,
        "name": name,
        "cod": cod,
      };
}

class CloudsModel extends Clouds {
  CloudsModel({
    @required int all,
  }) : super(all: all);

  factory CloudsModel.fromJson(Map<String, dynamic> json) => CloudsModel(
        all: json["all"],
      );

  Map<String, dynamic> toJson() => {
        "all": all,
      };
}

class CoordModel extends Coord {
  CoordModel({
    @required double lon,
    @required double lat,
  }) : super(lon: lon, lat: lat);

  factory CoordModel.fromJson(Map<String, dynamic> json) => CoordModel(
        lon: json["lon"]?.toDouble(),
        lat: json["lat"]?.toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "lon": lon,
        "lat": lat,
      };
}

class MainModel extends Main {
  MainModel({
    @required double temp,
    @required int pressure,
    @required int humidity,
    @required double tempMin,
    @required double tempMax,
  }) : super(
          temp: temp,
          pressure: pressure,
          humidity: humidity,
          tempMin: tempMin,
          tempMax: tempMax,
        );

  factory MainModel.fromJson(Map<String, dynamic> json) => MainModel(
        temp: json["temp"]?.toDouble(),
        pressure: json["pressure"],
        humidity: json["humidity"],
        tempMin: json["temp_min"]?.toDouble(),
        tempMax: json["temp_max"]?.toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "temp": temp,
        "pressure": pressure,
        "humidity": humidity,
        "temp_min": tempMin,
        "temp_max": tempMax,
      };
}

class SysModel extends Sys {
  SysModel({
    @required int type,
    @required int id,
    @required double message,
    @required String country,
    @required int sunrise,
    @required int sunset,
  }) : super(
          type: type,
          id: id,
          message: message,
          country: country,
          sunrise: sunrise,
          sunset: sunset,
        );

  factory SysModel.fromJson(Map<String, dynamic> json) => SysModel(
        type: json["type"],
        id: json["id"],
        message: json["message"]?.toDouble(),
        country: json["country"],
        sunrise: json["sunrise"],
        sunset: json["sunset"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "id": id,
        "message": message,
        "country": country,
        "sunrise": sunrise,
        "sunset": sunset,
      };
}

class WeatherElementModel extends WeatherElement {
  WeatherElementModel({
    @required int id,
    @required String main,
    @required String description,
    @required String icon,
  }) : super(
          id: id,
          main: main,
          description: description,
          icon: icon,
        );

  factory WeatherElementModel.fromJson(Map<String, dynamic> json) =>
      WeatherElementModel(
        id: json["id"],
        main: json["main"],
        description: json["description"],
        icon: json["icon"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "main": main,
        "description": description,
        "icon": icon,
      };
}

class WindModel extends Wind {
  WindModel({
    @required double speed,
    @required int deg,
  }) : super(
          speed: speed,
          deg: deg,
        );

  factory WindModel.fromJson(Map<String, dynamic> json) => WindModel(
        speed: json["speed"]?.toDouble(),
        deg: json["deg"],
      );

  Map<String, dynamic> toJson() => {
        "speed": speed,
        "deg": deg,
      };
}
