import 'dart:convert';

import 'package:weather_tdd/core/error/exception.dart';
import 'package:weather_tdd/features/weather/data/models/weather_model.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

abstract class WeatherRemoteDataSource {
  /// Call the https://openweathermap.org/data/2.5/weather?q={city}&appid=b6907d289e10d714a6e88b30761fae22 endpoint
  ///
  /// Throws a [ServerException] for all error codes.
  Future<WeatherModel> getWeatherCity(String city);
}

class WeatherRemoteDataSourceImpl implements WeatherRemoteDataSource {
  final http.Client client;

  WeatherRemoteDataSourceImpl({@required this.client});

  @override
  Future<WeatherModel> getWeatherCity(String city) async {
    Map<String, dynamic> queryParameters = {
      'q': city,
      'appid': 'e96908ec3090a6f13bfa7958f95220e7',
    };
    queryParameters.removeWhere((String key, dynamic value) => value == null);

    var uri = Uri(
        scheme: 'https',
        queryParameters: queryParameters,
        host: 'api.openweathermap.org',
        path: '/data/2.5/weather');

    final response = await client.get(
      uri,
      headers: {'Content-Type': 'application/json'},
    );
    print(response.body);
    print(response.statusCode);
    print(uri);
    if (response.statusCode == 200) {
      try {
        return WeatherModel.fromJson(json.decode(response.body));
      } catch (e) {
        throw NotFoundException();
      }
    } else {
      throw ServerException();
    }
  }
}
