import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:weather_tdd/core/error/failures.dart';
import 'package:weather_tdd/core/usecases/usecase.dart';
import 'package:weather_tdd/features/weather/domain/entities/weather.dart';
import 'package:meta/meta.dart';
import 'package:weather_tdd/features/weather/domain/repositories/weather_repository.dart';

class GetWeatherCity implements Usecase<Weather, Params> {
  final WeatherRepository repository;

  GetWeatherCity(this.repository);

  @override
  Future<Either<Failure, Weather>> call(Params params) async {
    return await repository.getWeatherCity(params.city);
  }
}

class Params extends Equatable {
  final String city;

  Params({@required this.city});

  @override
  List<Object> get props => [city];
}
