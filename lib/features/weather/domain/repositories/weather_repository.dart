import 'package:dartz/dartz.dart';
import 'package:weather_tdd/core/error/failures.dart';
import 'package:weather_tdd/features/weather/domain/entities/weather.dart';

abstract class WeatherRepository {
  Future<Either<Failure, Weather>> getWeatherCity(String city);
}