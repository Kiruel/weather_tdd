import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Weather extends Equatable {
  final Coord coord;
  final List<WeatherElement> weather;
  final String base;
  final Main main;
  final int visibility;
  final Wind wind;
  final Clouds clouds;
  final int dt;
  final Sys sys;
  final int id;
  final String name;
  final int cod;

  Weather({
    @required this.coord,
    @required this.weather,
    @required this.base,
    @required this.main,
    @required this.visibility,
    @required this.wind,
    @required this.clouds,
    @required this.dt,
    @required this.sys,
    @required this.id,
    @required this.name,
    @required this.cod,
  });

  @override
  List<Object> get props => [
        coord,
        weather,
        base,
        main,
        visibility,
        wind,
        clouds,
        dt,
        sys,
        id,
        name,
        cod,
      ];
}

class Clouds extends Equatable {
  final int all;

  Clouds({
    @required this.all,
  });

  @override
  List<Object> get props => [all];
}

class Coord extends Equatable {
  final double lon;
  final double lat;

  Coord({
    @required this.lon,
    @required this.lat,
  });

  @override
  List<Object> get props => [lon, lat];
}

class Main extends Equatable {
  final double temp;
  final int pressure;
  final int humidity;
  final double tempMin;
  final double tempMax;

  Main({
    @required this.temp,
    @required this.pressure,
    @required this.humidity,
    @required this.tempMin,
    @required this.tempMax,
  });

  @override
  List<Object> get props => [
        temp,
        pressure,
        humidity,
        tempMin,
        tempMax,
      ];
}

class Sys extends Equatable {
  final int type;
  final int id;
  final double message;
  final String country;
  final int sunrise;
  final int sunset;

  Sys({
    @required this.type,
    @required this.id,
    @required this.message,
    @required this.country,
    @required this.sunrise,
    @required this.sunset,
  });

  @override
  List<Object> get props => [
        type,
        id,
        message,
        country,
        sunrise,
        sunset,
      ];
}

class WeatherElement extends Equatable {
  final int id;
  final String main;
  final String description;
  final String icon;

  WeatherElement({
    @required this.id,
    @required this.main,
    @required this.description,
    @required this.icon,
  });

  @override
  List<Object> get props => [
        id,
        main,
        description,
        icon,
      ];
}

class Wind extends Equatable {
  final double speed;
  final int deg;

  Wind({
    @required this.speed,
    @required this.deg,
  });

  @override
  List<Object> get props => [
        speed,
        deg,
      ];
}
