class ServerException implements Exception {}

class CacheException implements Exception {}

class NoLocalDataException implements Exception {}

class NotFoundException implements Exception {}