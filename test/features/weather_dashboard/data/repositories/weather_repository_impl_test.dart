import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:weather_tdd/core/error/exception.dart';
import 'package:weather_tdd/core/error/failures.dart';
import 'package:weather_tdd/core/network/network_info.dart';
import 'package:weather_tdd/features/weather/data/datasources/weather_local_data_source.dart';
import 'package:weather_tdd/features/weather/data/datasources/weather_remote_data_source.dart';
import 'package:weather_tdd/features/weather/data/models/weather_model.dart';
import 'package:weather_tdd/features/weather/data/repositories/weather_repository_impl.dart';
import 'package:weather_tdd/features/weather/domain/entities/weather.dart';

class MockWeatherRemoteDataSource extends Mock
    implements WeatherRemoteDataSource {}

class MockWeatherLocalDataSource extends Mock
    implements WeatherLocalDataSource {}

class MockNetworkInfo extends Mock implements NetworkInfo {}

void main() {
  WeatherRepositoryImpl repository;
  MockWeatherRemoteDataSource mockWeatherRemoteDataSource;
  MockWeatherLocalDataSource mockWeatherLocalDataSource;
  MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockWeatherRemoteDataSource = MockWeatherRemoteDataSource();
    mockWeatherLocalDataSource = MockWeatherLocalDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = WeatherRepositoryImpl(
      remoteDataSource: mockWeatherRemoteDataSource,
      localDataSource: mockWeatherLocalDataSource,
      networkInfo: mockNetworkInfo,
    );
  });

  group('getWeatherCity', () {
    final tCloudsModel = CloudsModel(
      all: 90,
    );

    final tCoordModel = CoordModel(
      lon: -0.13,
      lat: 51.51,
    );

    final tMainModel = MainModel(
      temp: 280.32,
      pressure: 1012,
      humidity: 81,
      tempMax: 281.15,
      tempMin: 279.15,
    );

    final tSysModel = SysModel(
      type: 1,
      message: 0.0103,
      country: "GB",
      sunrise: 1485762037,
      sunset: 1485794875,
      id: 5091,
    );

    final tWeatherElementModel = WeatherElementModel(
      id: 300,
      main: "Drizzle",
      description: "light intensity drizzle",
      icon: "09d",
    );

    final tListWeatherElementModel = <WeatherElementModel>[
      tWeatherElementModel,
    ];

    final tWind = WindModel(
      deg: 80,
      speed: 4.1,
    );

    final tWeatherModel = WeatherModel(
      base: "stations",
      clouds: tCloudsModel,
      cod: 200,
      coord: tCoordModel,
      dt: 1485789600,
      id: 2643743,
      main: tMainModel,
      name: "London",
      sys: tSysModel,
      visibility: 10000,
      weather: tListWeatherElementModel,
      wind: tWind,
    );

    final tCity = 'London';

    final Weather tWeather = tWeatherModel;

    test(
      'should check if the device is online',
      () async {
        // arrange
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
        // act
        repository.getWeatherCity(tCity);
        // assert
        verify(mockNetworkInfo.isConnected);
      },
    );

    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      test(
        'should return remote data when the call to remote data is successful',
        () async {
          // arrange
          when(mockWeatherRemoteDataSource.getWeatherCity(tCity))
              .thenAnswer((_) async => tWeatherModel);
          // act
          final result = await repository.getWeatherCity(tCity);
          // assert
          verify(mockWeatherRemoteDataSource.getWeatherCity(tCity));
          expect(result, equals(Right(tWeather)));
        },
      );

      test(
        'should cache the data localy when the call to remote data source is successful',
        () async {
          // arrange
          when(mockWeatherRemoteDataSource.getWeatherCity(tCity))
              .thenAnswer((_) async => tWeatherModel);
          // act
          await repository.getWeatherCity(tCity);
          // assert
          verify(mockWeatherRemoteDataSource.getWeatherCity(tCity));
          verify(mockWeatherLocalDataSource.cacheWeather(tWeather));
        },
      );

      test(
        'should return server failure when the call to remote data source is unsuccessful',
        () async {
          // arrange
          when(mockWeatherRemoteDataSource.getWeatherCity(tCity))
              .thenThrow(ServerException());
          // act
          final result = await repository.getWeatherCity(tCity);
          // assert
          verify(mockWeatherRemoteDataSource.getWeatherCity(tCity));
          verifyZeroInteractions(mockWeatherLocalDataSource);
          expect(result, equals(Left(ServerFailure())));
        },
      );

      test(
        'should return a NotFoundFailure when the call from remote server can not parse the json',
        () async {
          // arrange
          when(mockWeatherRemoteDataSource.getWeatherCity(tCity)).thenThrow(NotFoundException());
          // act
          final result = await repository.getWeatherCity(tCity);
          // assert
          verify(mockWeatherRemoteDataSource.getWeatherCity(tCity));
          verifyZeroInteractions(mockWeatherLocalDataSource);
          expect(result, equals(Left(NotFoundFailure())));
        },
      );
    });

    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });

      test(
        'should return the last localy cached data when the cached data is present',
        () async {
          // arrange
          when(mockWeatherLocalDataSource.getLastWeather())
              .thenAnswer((_) async => tWeatherModel);
          // act
          final result = await repository.getWeatherCity(tCity);
          // assert
          verifyZeroInteractions(mockWeatherRemoteDataSource);
          verify(mockWeatherLocalDataSource.getLastWeather());
          expect(result, equals(Right(tWeather)));
        },
      );

      test(
        'should return a NoLocalDataFailure when there is no cached data present',
        () async {
          // arrange
          when(mockWeatherLocalDataSource.getLastWeather())
              .thenThrow(NoLocalDataException());
          // act
          final result = await repository.getWeatherCity(tCity);
          // assert
          verifyZeroInteractions(mockWeatherRemoteDataSource);
          verify(mockWeatherLocalDataSource.getLastWeather());
          expect(result, equals(Left(NoLocalDataFailure())));
        },
      );

      test(
        'should return a cache failure when cache can not be find',
        () async {
          // arrange
          when(mockWeatherLocalDataSource.getLastWeather())
              .thenThrow(CacheException());
          // act
          final result = await repository.getWeatherCity(tCity);
          // assert
          verifyZeroInteractions(mockWeatherRemoteDataSource);
          verify(mockWeatherLocalDataSource.getLastWeather());
          expect(result, equals(Left(CacheFailure())));
        },
      );
    });
  });
}
