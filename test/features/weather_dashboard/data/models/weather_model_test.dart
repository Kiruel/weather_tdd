import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:weather_tdd/features/weather/data/models/weather_model.dart';
import 'package:weather_tdd/features/weather/domain/entities/weather.dart';

import '../../../../fixtures/fixtures_reader.dart';

void main() {
  final tCloudsModel = CloudsModel(
    all: 90,
  );

  final tCoordModel = CoordModel(
    lon: -0.13,
    lat: 51.51,
  );

  final tMainModel = MainModel(
    temp: 280.32,
    pressure: 1012,
    humidity: 81,
    tempMax: 281.15,
    tempMin: 279.15,
  );

  final tSysModel = SysModel(
    type: 1,
    message: 0.0103,
    country: "GB",
    sunrise: 1485762037,
    sunset: 1485794875,
    id: 5091,
  );

  final tWeatherElementModel = WeatherElementModel(
    id: 300,
    main: "Drizzle",
    description: "light intensity drizzle",
    icon: "09d",
  );

  final tListWeatherElementModel = <WeatherElementModel>[
    tWeatherElementModel,
  ];

  final tWind = WindModel(
    deg: 80,
    speed: 4.1,
  );

  final tWeatherModel = WeatherModel(
    base: "stations",
    clouds: tCloudsModel,
    cod: 200,
    coord: tCoordModel,
    dt: 1485789600,
    id: 2643743,
    main: tMainModel,
    name: "London",
    sys: tSysModel,
    visibility: 10000,
    weather: tListWeatherElementModel,
    wind: tWind,
  );

  void testSubClass(dynamic actual, dynamic matcher) {
    test(
      'should be a subclass of ${matcher.runtimeType} entity',
      () async {
        // assert
        expect(actual, matcher);
      },
    );
  }

  group('subclassOrEntity', () {
    testSubClass(tWeatherModel, isA<Weather>());
    testSubClass(tCloudsModel, isA<Clouds>());
    testSubClass(tCoordModel, isA<Coord>());
    testSubClass(tMainModel, isA<Main>());
    testSubClass(tSysModel, isA<Sys>());
    testSubClass(tListWeatherElementModel.first, isA<WeatherElement>());
    testSubClass(tWind, isA<Wind>());
  });

  group('fromJson', () {
    test(
      'should return a valid model when the JSON weather is a Weather',
      () async {
        // arrange
        final Map<String, dynamic> jsonMap =
            json.decode(fixture('weather.json'));
        // act
        final result = WeatherModel.fromJson(jsonMap);
        // assert
        expect(result, tWeatherModel);
      },
    );
  });

  group('toJson', () {
    test(
      'should return a JSON map containing the proper data',
      () async {
        // act
        final result = tWeatherModel.toJson();
        // assert
        final expectedJsonMap = {
          "coord": {"lon": -0.13, "lat": 51.51},
          "weather": [
            {
              "id": 300,
              "main": "Drizzle",
              "description": "light intensity drizzle",
              "icon": "09d"
            }
          ],
          "base": "stations",
          "main": {
            "temp": 280.32,
            "pressure": 1012,
            "humidity": 81,
            "temp_min": 279.15,
            "temp_max": 281.15
          },
          "visibility": 10000,
          "wind": {"speed": 4.1, "deg": 80},
          "clouds": {"all": 90},
          "dt": 1485789600,
          "sys": {
            "type": 1,
            "id": 5091,
            "message": 0.0103,
            "country": "GB",
            "sunrise": 1485762037,
            "sunset": 1485794875
          },
          "id": 2643743,
          "name": "London",
          "cod": 200
        };

        expect(result, expectedJsonMap);
      },
    );
  });
}
