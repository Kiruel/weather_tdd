import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_tdd/core/error/exception.dart';
import 'package:weather_tdd/features/weather/data/datasources/weather_local_data_source.dart';
import 'package:weather_tdd/features/weather/data/models/weather_model.dart';
import 'package:matcher/matcher.dart';

import '../../../../fixtures/fixtures_reader.dart';

class MockSharedPreference extends Mock implements SharedPreferences {}

void main() {
  WeatherLocalDataSourceImpl dataSource;
  MockSharedPreference mockSharedPreference;

  setUp(() {
    mockSharedPreference = MockSharedPreference();
    dataSource = WeatherLocalDataSourceImpl(
      sharedPreferences: mockSharedPreference,
    );
  });

  group('getLastWeater', () {
    final tWeatherModel =
        WeatherModel.fromJson(json.decode(fixture('weather_cached.json')));

    test(
      'should return Weather from SharedPreferences when there is one in the cache',
      () async {
        // arrange
        when(mockSharedPreference.getString(any))
            .thenReturn(fixture('weather_cached.json'));
        // act
        final result = await dataSource.getLastWeather();
        // assert
        verify(mockSharedPreference.getString(CACHED_WEATHER));
        expect(result, equals(tWeatherModel));
      },
    );

    test(
      'should throw a NoLocalDataException when there is not a cached value',
      () async {
        // arrange
        when(mockSharedPreference.getString(any)).thenReturn(null);
        // act
        final call = dataSource.getLastWeather;
        // assert
        expect(() => call(), throwsA(TypeMatcher<NoLocalDataException>()));
      },
    );
  });

  group('cacheWeather', () {
    final tWeatherModel = WeatherModel(
      base: "stations",
      clouds: CloudsModel(
        all: 90,
      ),
      cod: 200,
      coord: CoordModel(
        lon: -0.13,
        lat: 51.51,
      ),
      dt: 1485789600,
      id: 2643743,
      main: MainModel(
        temp: 280.32,
        pressure: 1012,
        humidity: 81,
        tempMax: 281.15,
        tempMin: 279.15,
      ),
      name: "London",
      sys: SysModel(
        type: 1,
        message: 0.0103,
        country: "GB",
        sunrise: 1485762037,
        sunset: 1485794875,
        id: 5091,
      ),
      visibility: 10000,
      weather: <WeatherElementModel>[
        WeatherElementModel(
          id: 300,
          main: "Drizzle",
          description: "light intensity drizzle",
          icon: "09d",
        ),
      ],
      wind: WindModel(
        deg: 80,
        speed: 4.1,
      ),
    );

    test(
      'should call SharedPrerences to cache the data',
      () async {
        // act
        dataSource.cacheWeather(tWeatherModel);
        // assert
        final expectedJsonString = json.encode(tWeatherModel.toJson());
        verify(mockSharedPreference.setString(
          CACHED_WEATHER,
          expectedJsonString,
        ));
      },
    );
  });
}
