import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:http/http.dart' as http;
import 'package:weather_tdd/core/error/exception.dart';
import 'package:weather_tdd/features/weather/data/datasources/weather_remote_data_source.dart';
import 'package:weather_tdd/features/weather/data/models/weather_model.dart';
import 'package:matcher/matcher.dart';

import '../../../../fixtures/fixtures_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  WeatherRemoteDataSourceImpl dataSource;
  MockHttpClient mockHttpClient;

  setUp(() {
    mockHttpClient = MockHttpClient();
    dataSource = WeatherRemoteDataSourceImpl(client: mockHttpClient);
  });

  void setUpMockHttpClientSuccess200() {
    when(mockHttpClient.get(any, headers: anyNamed('headers')))
        .thenAnswer((_) async => http.Response(fixture('weather.json'), 200));
  }

  void setUpMockHttpClientFail404() {
    when(mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
      (_) async => http.Response('Something went wrong', 404),
    );
  }

  group('getWeatherCity', () {
    final tCity = 'London';
    final tWeatherModel =
        WeatherModel.fromJson(json.decode(fixture('weather.json')));
    test(
      'should perform a GET request on a URL with city being the enpoint and with application/json header',
      () async {
        Map<String, dynamic> queryParameters = {
          'q': tCity,
          'appid': 'e96908ec3090a6f13bfa7958f95220e7',
        };
        queryParameters
            .removeWhere((String key, dynamic value) => value == null);

        var uri = Uri(
            scheme: 'https',
            queryParameters: queryParameters,
            host: 'api.openweathermap.org',
            path: '/data/2.5/weather');

        // arrange
        setUpMockHttpClientSuccess200();
        // act
        dataSource.getWeatherCity(tCity);
        // assert
        verify(mockHttpClient.get(
          uri,
          headers: {'Content-Type': 'application/json'},
        ));
      },
    );

    test(
      'should return Weather when the response code is 200 (success)',
      () async {
        // arrange
        setUpMockHttpClientSuccess200();
        // act
        final result = await dataSource.getWeatherCity(tCity);
        // assert
        expect(result, tWeatherModel);
      },
    );

    test(
      'should throw a ServerException when the response code is 404 or other',
      () async {
        // arrange
        setUpMockHttpClientFail404();
        // act
        final call = dataSource.getWeatherCity;
        // assert
        expect(() => call(tCity), throwsA(TypeMatcher<ServerException>()));
      },
    );

    test(
      'should throw a ServerException when the response can not be parsed',
      () async {
        // arrange
        when(mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
            (_) async => http.Response('Something went wrong', 200));
        // act
        final call = dataSource.getWeatherCity;
        // assert
        expect(() => call(tCity), throwsA(TypeMatcher<NotFoundException>()));
      },
    );
  });
}
