import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:weather_tdd/features/weather/domain/entities/weather.dart';
import 'package:weather_tdd/features/weather/domain/repositories/weather_repository.dart';
import 'package:mockito/mockito.dart';
import 'package:weather_tdd/features/weather/domain/usercases/get_weather_city.dart';

class MockWeatherRepository extends Mock implements WeatherRepository {}

void main() {
  GetWeatherCity usecase;
  MockWeatherRepository mockWeatherRepository;

  setUp(() {
    mockWeatherRepository = MockWeatherRepository();
    usecase = GetWeatherCity(mockWeatherRepository);
  });

  final tCity = 'London';
  final tWeather = Weather(
    base: '',
    clouds: Clouds(
      all: 0,
    ),
    cod: 0,
    coord: Coord(
      lon: 0.0,
      lat: 0.0,
    ),
    dt: 0,
    id: 0,
    main: Main(
      temp: 0.0,
      pressure: 0,
      humidity: 0,
      tempMax: 0.0,
      tempMin: 0.0,
    ),
    name: '',
    sys: Sys(
      type: 0,
      message: 0.0,
      country: '',
      sunrise: 0,
      sunset: 0,
      id: 0,
    ),
    visibility: 0,
    weather: <WeatherElement>[],
    wind: Wind(
      deg: 0,
      speed: 0.0,
    ),
  );

  test(
    'should get the weather for a city from the repository',
    () async {
      // arrange
      when(mockWeatherRepository.getWeatherCity(any))
          .thenAnswer((_) async => Right(tWeather));
      // act
      final result = await usecase(Params(city: tCity));
      // assert
      expect(result, Right(tWeather));
      verify(mockWeatherRepository.getWeatherCity(tCity));
      verifyNoMoreInteractions(mockWeatherRepository);
    },
  );
}
