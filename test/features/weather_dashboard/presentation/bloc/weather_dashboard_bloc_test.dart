import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:weather_tdd/core/error/failures.dart';
import 'package:weather_tdd/features/weather/domain/entities/weather.dart';
import 'package:weather_tdd/features/weather/domain/usercases/get_weather_city.dart';
import 'package:weather_tdd/features/weather/presentation/bloc/bloc.dart';

class MockGetWeatherCity extends Mock implements GetWeatherCity {}

void main() {
  WeatherBloc bloc;
  MockGetWeatherCity mockGetWeatherCity;

  setUp(() {
    mockGetWeatherCity = MockGetWeatherCity();
    bloc = WeatherBloc(city: mockGetWeatherCity);
  });

  test(
    'initialState should be Empty',
    () async {
      // assert
      expect(bloc.state, equals(Empty()));
    },
  );

  group('GetWeatherForCity', () {
    final String tCity = 'London';

    final tWeather = Weather(
      base: "stations",
      clouds: Clouds(
        all: 90,
      ),
      cod: 200,
      coord: Coord(
        lon: -0.13,
        lat: 51.51,
      ),
      dt: 1485789600,
      id: 2643743,
      main: Main(
        temp: 280.32,
        pressure: 1012,
        humidity: 81,
        tempMax: 281.15,
        tempMin: 279.15,
      ),
      name: "London",
      sys: Sys(
        type: 1,
        message: 0.0103,
        country: "GB",
        sunrise: 1485762037,
        sunset: 1485794875,
        id: 5091,
      ),
      visibility: 10000,
      weather: <WeatherElement>[
        WeatherElement(
          id: 300,
          main: "Drizzle",
          description: "light intensity drizzle",
          icon: "09d",
        ),
      ],
      wind: Wind(
        deg: 80,
        speed: 4.1,
      ),
    );

    test(
      'should get data from the city use case',
      () async {
        // arrange
        when(mockGetWeatherCity(any)).thenAnswer((_) async => Right(tWeather));
        // act
        bloc.add(GetWeatherForCity(tCity));
        await untilCalled(mockGetWeatherCity(any));
        // assert
        verify(mockGetWeatherCity(Params(city: tCity)));
      },
    );

    test(
      'should emit [Loading, Loaded] when data is goten successfully',
      () async {
        // arrange
        when(mockGetWeatherCity(any)).thenAnswer((_) async => Right(tWeather));

        // assert later
        final expected = [
          Loading(),
          Loaded(weather: tWeather),
        ];
        expectLater(bloc.stream, emitsInOrder(expected));
        // act
        bloc.add(GetWeatherForCity(tCity));
      },
    );

    test(
      'should emit [Loading, Error] when getting data fails',
      () async {
        // arrange
        when(mockGetWeatherCity(any))
            .thenAnswer((_) async => Left(ServerFailure()));
        // assert later
        final expected = [
          Loading(),
          Error(message: SERVER_FAILURE_MESSAGE),
        ];
        expectLater(bloc.stream, emitsInOrder(expected));
        // act
        bloc.add(GetWeatherForCity(tCity));
      },
    );

    test(
      'should emit [Loading, Error] with a proper message for the error when getting data fails',
      () async {
        // arrange
        when(mockGetWeatherCity(any))
            .thenAnswer((_) async => Left(NoLocalDataFailure()));
        // assert later
        final expected = [
          Loading(),
          Error(message: CACHE_FAILURE_MESSAGE),
        ];
        expectLater(bloc.stream, emitsInOrder(expected));
        // act
        bloc.add(GetWeatherForCity(tCity));
      },
    );

    test(
      'should emit [Loading, Error] with a proper message when no city found',
      () async {
        // arrange
        when(mockGetWeatherCity(any))
            .thenAnswer((_) async => Left(NotFoundFailure()));
        // assert later
        final expected = [
          Loading(),
          Error(message: NOT_FOUND_FAILURE_MESSAGE),
        ];
        expectLater(bloc.stream, emitsInOrder(expected));
        // act
        bloc.add(GetWeatherForCity(tCity));
      },
    );
  });
}
